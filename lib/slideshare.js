var _ = require('underscore'),
  qs = require('qs'),
  http = require('http'),
  crypto = require('crypto'),
  xml2json = require('xml2json');

/**
 * SlideShare API hostname
 * @const
 */
var API_HOST = 'www.slideshare.net';

/**
 * Create required API parameters.
 *
 * @return {Object} Required API parameters (api_key, ts, hash)
 */
var createBaseParameters = function() {
  var ts = String(Math.floor(Date.now() / 1000)),
      sha1 = crypto.createHash('sha1'),
      params = {
        api_key: process.env.SLIDESHARE_KEY,
        ts: ts,
        hash: sha1.update(process.env.SLIDESHARE_SECRET + ts).digest('hex')
      };
  return params;
};

var createQuery = function(query) {
  var params = createBaseParameters();
  params.detailed = '1';
  _.extend(params, query);
  return qs.stringify(params);
};

var searchSlides = function(query) {
  http.get({
    host: API_HOST,
    path:'/api/2/search_slideshows?' + createQuery({ q:query })
  }, function (res) {
    var buf = '';
    res.on('data', function (chunk) {
      buf += chunk;
    });
    res.on('end', function () {
      console.log(xml2json.toJson(buf));
    });
  });
};

var getSlideShowsByUser = function(options, callback) {
  http.get({
    host: API_HOST,
    path:'/api/2/get_slideshows_by_user?' + createQuery(options)
  }, function (res) {
    var buf = '';
    res.on('data', function (chunk) {
      buf += chunk;
    });
    res.on('end', function () {
      var json = xml2json.toJson(buf);
      var userInfo = JSON.parse(json);
      callback(null, userInfo.User);
    });
  }).on('error', function(e) {
    callback(e);
  });
}

// Exports
exports.searchSlides = searchSlides;
exports.getSlideShowsByUser = getSlideShowsByUser;
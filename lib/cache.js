/**
 * Module dependencies.
 */
var redis = require("redis"),
    url = require('url');

/**
 * Initialize Cache with the given 'options'.
 * Return the cache using Redis.
 *
 * @param {String} uri Redis server uri
 * @public
 */
var Cache = function(uri) {
  var host = url.parse(uri),
      options = { host: host.hostname };

  if (host.port) {
    options.port = host.port;
  }
  if (host.auth) {
    options.pass = host.auth.split(':')[1];
  }

  console.log('Cache servier is %j', options);
  this.client = new redis.createClient(options.port, options.host, options);
  this.client.on("error", function (err) {
    console.error(err);
  });

  if (options.pass) {
    this.client.auth(options.pass, function(err) {
      if (err) {
        console.error(err);
        throw err;
      }
    });
  }

  if (options.db) {
    var self = this;
    self.client.select(options.db);
    self.client.on('connect', function() {
      self.client.send_anyway = true;
      self.client.select(options.db);
      self.client.send_anyway = false;
    });
  }
};

/**
 * Get object from cache the by given 'key'.
 *
 * @param {String} key key of object
 * @param {Function} callback callback function
 */
Cache.prototype.get = function(key, callback) {
  callback = callback || function(){};
  this.client.get(key, function(err, data) {
    if (err) return callback(err);

    try {
      if (!data) {
        return callback(null, data);
      }
      callback(null, JSON.parse(data.toString()));
    } catch(e) {
      callback(e);
    }
  });
};

/**
 * Set object to cache associated with the given 'key'.
 *
 * @param {String} key
 * @param {Object} value
 * @param {Number} ttl
 * @param {Function} callback
 */
Cache.prototype.set = function(key, value, ttl, callback) {
  callback = callback || function(){};
  try {
    var t = isNaN(ttl)? parseInt(ttl, 10) : ttl;
    if (isNaN(t) || t <= 0) {
      this.client.set(key, JSON.stringify(value), function() {
        callback.apply(this, arguments);
      });
    } else {
      this.client.setex(key, ttl, JSON.stringify(value), function() {
        callback.apply(this, arguments);
      });
    }
  } catch(e) {
    callback(e);
  }
};

/**
 * Remove object from cache by the given 'key'.
 *
 * @param {String} key
 * @param {Function} callback
 */
Cache.prototype.remove = function(key, callback) {
  this.client.del(key, callback);
};

/**
 * Clear all cached objects.
 *
 * @param {Function} callback
 */
Cache.prototype.clear = function(callback) {
  this.client.flushdb(callback);
};

module.exports = new Cache(process.env.REDISTOGO_URL || 'redis://localhost');
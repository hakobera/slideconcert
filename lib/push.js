var sio = require('socket.io'),
    cache = require('./cache');

var io;

var rooms = {};

exports.listen = function(app) {
  //var redisStore = new sio.RedisStore({ redisClient: cache.client });
  //io = sio.listen(app, { store: redisStore });

  io = sio.listen(app);

  io.configure('production', function(){
    io.set('browser client minification', true);
    io.set('log level', 1);
    io.set('transports', [ 'xhr-polling' ]);
    io.set('polling duration', 10);
  });

  io.configure('development', function(){
    io.set('transports', ['websocket']);
  });
};

exports.open = function(roomId, docId) {
  var room = rooms[roomId];
  if (room) {
    room.docId = docId;
    broadcast(roomId, 'open', { docId: docId });
  }
}

var broadcast = exports.broadcast = function(roomId, event, data) {
  console.log(roomId, event, data);

  var room = rooms[roomId];
  if (room) {
    room.volatile.emit(event, data);
  }
};

exports.createRoom = function(roomId) {
  var room = rooms[roomId];
  if (room) {
    return room;
  }

  room = io
    .of(roomId)
    .on('connection', function(socket) {
      socket.emit('join', { roomId: roomId, docId: room.docId });
    });
  rooms[roomId] = room;
  return room;
}
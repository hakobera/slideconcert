var redis = require('connect-redis'),
    express = require('express'),
    url = require('url');

exports.configure = function(app) {
  var RedisStore = redis(express),
      redisUri = url.parse(process.env.REDISTOGO_URL || 'redis://localhost/'),
      options = { host: redisUri.hostname },
      store;

  if (redisUri.port) {
    options.port = redisUri.port;
  }

  if (redisUri.auth) {
    options.pass = redisUri.auth.split(':')[1];
  }

  store = new RedisStore(options);

  app.use(express.cookieParser());
  app.use(express.session({ secret: process.env.SESSION_SECRET || 'Jkel3k193+eizk;', store: store }));
};
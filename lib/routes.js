var slideshare = require('./slideshare'),
    auth = require('./auth'),
    push = require('./push');

/**
 * Bind app to expressApp.
 */
module.exports = function(app) {
  /**
   * Show live view.
   */
  app.get('/live/:userId', function(req, res, next) {
    var userId = req.param('userId'),
        roomId = '/' + userId;
    push.createRoom(roomId);
    res.render('live', { userId: userId });
  });

  /**
   * Find slides by user
   */
  app.get('/api/users/:userId/slides', function(req, res, next) {
    auth.loggedIn(req, function(loggedIn) {
      if (!loggedIn) {
        return res.send(401);
      }

      var query = {
          username_for: req.param('userId')
      };

      slideshare.getSlideShowsByUser(query, function(err, data) {
        if (err) return next(err);
        res.json(data);
      });
    });
  });

  /**
   * Control view from mobile client
   */
  app.get('/control/open/:userId/:docId', function(req, res, next) {
    auth.loggedIn(req, function(loggedIn, user) {
      if (!loggedIn) {
        return res.send(401);
      }

      var userId = req.param('userId');
      if (userId !== user.name) {
        return res.send(401);
      }

      var docId = req.param('docId'),
          roomId = '/' + userId,
          numSlides = req.param('numSlides');

      console.log(docId);

      push.createRoom(roomId);
      push.open(roomId, docId);

      res.render('control', { user: user, numSlides: numSlides });
    });
  });

  /**
   * Control view from mobile client
   */
  app.post('/control/command/:userId/:command', function(req, res, next) {
    auth.loggedIn(req, function(loggedIn, user) {
      if (!loggedIn) {
        return res.send(401);
      }

      var userId = req.param('userId');
      if (userId !== user.name) {
        console.log(user);
        console.log(userId);
        return res.send(401);
      }

      var roomId = '/' + userId,
          command = req.param('command'),
          body = req.body;

      push.broadcast(roomId, command, body);

      console.log(command, body);
      res.json({ command: command, status: 'accepted' });
    });
  });

  /**
   * Send comment
   */
  app.post('/comment/:roomId', function(req, res, next) {
    var roomId = '/' + req.param('roomId'),
        comment = req.param('comment');

    push.broadcast(roomId, 'comment', { comment: comment });
    res.json({ comment: comment, status: 'accepted' });
  });

}
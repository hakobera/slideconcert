/*
 * Authentication module.
 */

var everyauth = require('everyauth'),
    util = require('util'),
    crypto = require('crypto'),
    cache = require('./cache');

var ACCESS_SECRET = 'KL397keica3jI38kdive+kd4jkILQEIXO';

function findUserById(id, callback) {
  cache.get(id, callback);
};

/**
 * Check and return whether current user is logged in or not.
 *
 * @param req
 * @return {boolean} If user is logged in then return true
 */
var loggedIn = exports.loggedIn = function(req, callback) {
  findUserById(req.param('id'), function(err, user) {
    console.log(user);

    if (err) return callback(false);

    var date = req.param('date'),
        sign = req.param('sign');


    if (!user || !date || !sign) {
      return callback(false);
    }

    var verify =
      crypto
        .createHmac('sha1', ACCESS_SECRET)
        .update(user.id + ':' + date + ':' + user.token)
        .digest('hex');

    console.log(sign);
    console.log(verify);

    if (verify === sign) {
      console.log('%s logged in', user.id);
      user.sign = function() {
        this.date = Date.now();
        return crypto
          .createHmac('sha1', ACCESS_SECRET)
          .update(this.id + ':' + this.date + ':' + this.token)
          .digest('hex');
      };
      callback(true, user);
    } else {
      callback(false);
    }
  });
};

/**
 * Execute login sequence.
 *
 * @param req
 * @param res
 */
var login = exports.login = function login(req, res) {
  console.log(req.cookies);
  res.redirect(everyauth.twitter.entryPath());
};

/**
 * Initial configuration of auth module.
 *
 * @param {Object} app Express application instance
 */
exports.configure = function(app) {
  everyauth.everymodule
    .findUserById(findUserById);

  everyauth.twitter
    .consumerKey(process.env.TWITTER_CONSUMER_KEY)
    .consumerSecret(process.env.TWITTER_CONSUMER_SECRET)
    .findOrCreateUser( function (session, accessToken, accessTokenSecret, twitterUserMetadata) {
      var userId = twitterUserMetadata.id,
          promise = this.Promise();

      cache.get(userId, function(err, data) {
        //if (err) {
          var user = {
            id: userId,
            name: twitterUserMetadata.screen_name,
            token: crypto.createHmac('sha256', ACCESS_SECRET).update(userId + ':' + Date.now() + ':' + accessToken).digest('hex')
          };
          console.log('get %s', userId);
          cache.set(userId, user, 3600000, function(e) {
            console.log('set %s', userId);
            if (e) return promise.fail(e);
            promise.fulfill(user);
          });
        //} else {
        //  promise.fulfill(data);
        //}
      });

      return promise;
    })
    .handleAuthCallbackError(function(req, res) {
      res.send('<html><head><title>loginCancel</title></head><body></body></html>');
    })
    .redirectPath('/settings');

  app.use(everyauth.middleware());

  app.get('/auth', login);

  app.post('/loggedIn', function(req, res) {
    loggedIn(req, function(result) {
      var status = (result ? 200 : 401);
      res.send(status);
    });
  });

  app.get('/settings', function(req, res) {
    findUserById(req.session.auth.userId, function(err, user) {
      if (err) res.send(401);
      res.render('settings', { user: user });
    });
  });

};
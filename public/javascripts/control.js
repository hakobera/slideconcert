document.addEventListener('DOMContentLoaded', function() {
  function $(id) {
    return document.getElementById(id);
  }

  var roomId = $('roomId').value,
      userId = $('userId').value,
      date = $('date').value,
      sign = $('sign').value;

  var pageText = $('pageNum'),
      maxPage = parseInt($('maxPage').value, 10);


  var live = io.connect(roomId);
  live.on('join', function(data) {
    console.log(data);
  });

  function getPageNumber() {
    var pageNum = pageText.value;
    var page = parseInt(pageNum, 10);
    if (isNaN(page)) {
      alert('数字を入力してください');
      return -1;
    }
    return page;
  }

  function command(cmd, data, success) {
    var url = '/control/command/' + roomId + '/' + cmd;

    data = data || {};
    data.id = userId;
    data.date = date;
    data.sign = sign;

    var xhr = new XMLHttpRequest();
    xhr.open('POST', url);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4) {
        if (xhr.status === 200) {
          if (success) success(xhr.responseText);
        } else {
          alert(xhr.responseText);
        }
      }
    };
    xhr.send(JSON.stringify(data));
  }

  function jumpPage(page) {
    command('jump', { page: page }, function(data) {
      pageText.value = page;
    });
  }

  var prev = $('prev');
  prev.addEventListener('click', function(e) {
    var page = getPageNumber();
    if (page > 1) {
      jumpPage(page - 1);
    }
    return false;
  });

  var next = $('next');
  next.addEventListener('click', function(e) {
    var page = getPageNumber();
    if (page < maxPage) {
      jumpPage(page + 1);
    }
    return false;
  });

  var jump = $('jump');
  jump.addEventListener('click', function(e) {
    var page = getPageNumber();
    if (page > 0 && page <= maxPage) {
      jumpPage(page);
    }
    return false;
  });
});
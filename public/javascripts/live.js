$(function() {
  var path = window.location.pathname,
      roomId = path.substr(path.lastIndexOf('/')),
      container = $('#container'),
      title = $('#title'),
      comments = $('#comments'),
      live = io.connect(roomId),
      player;

  console.log(roomId);

  live.on('join', function(data) {
    if (data.docId) {
      loadSlide(data.docId);
    }
  });

  live.on('open', function(data) {
    loadSlide(data.docId);
  });

  live.on('next', function() {
    if (player) {
      player.next();
    }
  });

  live.on('prev', function() {
    if (player) {
      player.previous();
    }
  });

  live.on('jump', function(data) {
    if (player) {
      var page = data.page;
      player.jumpTo(page);
    }
  });

  live.on('comment', function(data) {
    comments.prepend($('<li>').text(data.comment));
  });

  $('#comment').keydown(function(event) {
    if (event.keyCode === 13) {
      sendComment();
      return false;
    }
  });

  $('#commentButton').click(function(e) {
    e.preventDefault();
    sendComment();
  });

  var commentSending = false;
  function sendComment() {
    if (commentSending) {
      return;
    }

    var comment = $('#comment').val();
    commentSending = true;

    $.post('/comment' + roomId, { comment: comment })
      .success(function() {

      })
      .error(function() {
        alert('Error');
      })
      .complete(function() {
        commentSending = false;
      });
  }

  //Load the flash player. Properties for the player can be changed here.
  function loadSlide(docId) {
    var params = { allowScriptAccess: "always", allowFullscreen: "true" };
    var atts = { id: "slideContainer" };
    var flashvars = { doc: docId, startSlide: 1, rel: 0 };

    player = null;
    title.text('Online');
    swfobject.embedSWF("http://static.slidesharecdn.com/swf/ssplayer2.swf", "slideContainer", "598", "480", "8", null, flashvars, params, atts);

    player = document.getElementById("slideContainer");
  }
});
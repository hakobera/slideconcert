
/**
 * Module dependencies.
 */

var express = require('express'),
    session = require('./lib/session'),
    auth = require('./lib/auth'),
    push = require('./lib/push'),
    routes = require('./lib/routes');

var port = process.env.PORT || 3000;

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.set('view options', { layout: false });
  session.configure(app);
  app.use(express.bodyParser());
  auth.configure(app);
  app.use(express.methodOverride());
  app.use(require('stylus').middleware({ src: __dirname + '/public' }));
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

push.listen(app);
routes(app);

app.listen(port);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
